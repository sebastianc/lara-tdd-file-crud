<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;

class GenJwtCommand extends Command
{
    use Notifiable;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gen:jwt';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate the jwt secret';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{

            $key = Str::random(32);

            /*if ($this->option('show')) {
                return $this->line('<comment>'.$key.'</comment>');
            }*/

            $path = config_path('jwt.php');

            if (file_exists($path)) {
                file_put_contents($path, str_replace(
                    $this->laravel['config']['jwt.secret'], $key, file_get_contents($path)
                ));
            }

            $this->laravel['config']['jwt.secret'] = $key;

            $this->info("jwt-auth secret [$key] set successfully.");

        }catch (\Exception $e){
            $this->info($e);
        }
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['show', null, InputOption::VALUE_NONE, 'Simply display the key instead of modifying files.'],
        ];
    }
}
