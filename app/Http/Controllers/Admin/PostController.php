<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Post;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class PostController extends Controller
{
    public function getPosts(Request $request) {
        $posts = Post::whereNull('deleted_at');
        if(isset($request['s'])){
            $searchText = $request['s'];
            $posts = $posts->where('title', 'LIKE', '%'.$searchText.'%')->orWhere('body', 'LIKE', '%'.$searchText.'%')
                ->orderBy('created_at', 'desc')->paginate(15);
            if($posts->count() === 0 ){
                return view('web.admin.posts', ['posts' => $posts, 'noResults' => $searchText]);
            }
            else{
                return view('web.admin.posts', ['posts' => $posts]);
            }
        }
        else{
            $posts = $posts->orderBy('id', 'desc')->paginate(15);
            return view('web.admin.posts', ['posts' => $posts]);
        }
    }

    public function viewPost($postId) {
        $post = Post::where('id', $postId)->whereNull('deleted_at')->first();

        if(isset($post->id)){
            return view('web.admin.viewPost', ['post' => $post]);
        } else {
            return redirect()->route('home')->with(['error_message' => 'Post not found or deleted.']);
        }

    }

    public function updatePost(Request $request, $postId){
        $post = Post::find($postId);

        if($post->user_id == Auth::user()->id){
            if (isset($request['title']) && isset($request['body'])){
                $post->title = $request['title'];
                $post->body = $request['body'];
                //$user = User::where('id',$request['user_id'])->first();
                $post->save();
            }

            //Possible future implementation for post images

            if( $request->hasFile('post_image') ) {
                $imageName = $request->file('post_image')->getClientOriginalName();
                $path = base_path() . '/public/uploads/images/';
                $request->file('post_image')->move($path , $imageName);
                $post->post_image = Image::make($path.$imageName);
                $post->save();
            }

            if (isset($request['delete'])){
                $post->delete();
                return redirect()->route('home')->with(['success_message' => 'Post successfully deleted.']);
            }

            return redirect()->route('viewPost', $postId)->with(['success_message' => 'Post successfully updated.']);
        } else {
            return redirect()->route('viewPost', $postId)->with(['error_message' => 'You are not the author of this post.']);
        }
    }

    public function addPost(Request $request){

        if (isset($request['title']) && isset($request['body'])){
            if(!empty($request['title']) && !empty($request['body'])){
            $post = new Post;
            $post->title = $request['title'];
            $post->body = $request['body'];
            //$user = User::where('id',$request['user_id'])->first();
            $post->user_id = Auth::user()->id;
            $post->save();
            } else {
                return redirect()->route('newPostPage')->with(['error_message' => 'Please specify both title and body.']);
            }
        }

        //Possible future implementation for post images

        if( $request->hasFile('post_image') ) {
            $imageName = $request->file('post_image')->getClientOriginalName();
            $path = base_path() . '/public/uploads/images/';
            $request->file('post_image')->move($path , $imageName);
            $post->post_image = Image::make($path.$imageName);
            $post->save();
        }

        return redirect()->route('home')->with(['success_message' => 'Post successfully created.']);

    }

    public function newPostPage(Request $request)
    {
        return view('web.admin.newPost');
    }

}
