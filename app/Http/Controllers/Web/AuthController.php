<?php

namespace App\Http\Controllers\Web;


use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Cache\RateLimiter;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    //use ThrottlesLogins, AuthenticatesUsers;
    use AuthenticatesUsers;
    protected $maxLoginAttempts = 5;
    protected $lockoutTime = 600;

    /**
     * Register new user
     * @param Request $request
     * @return mixed
     */
    public function register(Request $request)
    {
       try{
           $data = Input::only('email', 'password', 'name', 'username', 'phone');


           $validator = Validator::make($request->all(), [
               'email' => 'required|email|unique:users|max:255',
               'password' => 'required|min:8',
               'name' => 'required|min:4|max:56|unique:users',
               'username' => 'required|min:4|max:56|unique:users',
           ]);
           if ($validator->fails()) {
               //return json_encode(['status' => 'failed', 'error' => $validator->errors()->first()]);
               return redirect()->back()->with('error_message', $validator->errors()->first());
           } else {
               $new_user = new User;
               $new_user->email = $data['email'];
               $new_user->password = bcrypt($data['password']);
               $new_user->name = $data['name'];
               $new_user->username = $data['username'];
               $new_user->phone = $data['phone'];

               if($new_user->save()) {
                   Auth::login($new_user);
                   return redirect('/dashboard/home')->with('success_message' , 'Welcome to Xanadu the dream portal!');
               }

           }
       }catch (\Exception $e){
           dd($e);
       }
    }

    function registerPage()
    {

        if(isset(Auth::user()->id)){
            return redirect('/dashboard/home');
        }

        return view('web.pages.register');
    }

    /**
     * Login user
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login( Request $request)
    {
        // grab credentials from the request
        $credentials = Input::only('email', 'password');

        try {
            //If too many login attempts block IP
            if ($this->hasTooManyLoginAttempts($request)) {
                return redirect('/dashboard/login')->with('error_message' , 'Too many failed login attempts');
            }
            // attempt to verify the credentials for the user
            if (!Auth::attempt($credentials)) {
                /*
                 * Future functionality for password resetting
                 *
                $user = User::where('email', $credentials['email'])->first();

                if($user){
                    return redirect('/dashboard/login')->with(['parse_user_login' => true , 'error_message' => 'Please reset your password.']);
                }
                */
                //Failed so increment attempts
                $this->incrementLoginAttempts($request);
                return redirect('/dashboard/login')->with('error_message' , 'Incorrect email or password')->with('attempts_remaining' , $this->retriesLeft($request));
            }
        } catch (\Exception $e) {
            // something went wrong whilst attempting to login user
            return redirect('/dashboard/login')->with('error_message' , 'Something went wrong, please try again later.');

        }
        // all good so return the user and reset login attempts
        $this->clearLoginAttempts($request);
        $user = User::find(Auth::user()->id);
        return redirect('/dashboard/home')->with('success_message' , 'Welcome to Xanadu the dream portal!')->with('user', $user);

    }

    /**
     * View login page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function loginPage(Request $request)
    {

        if(isset(Auth::user()->id)){
            return redirect('/dashboard/home');
        }

        return view('web.pages.login');
    }

    /**
     * Logout user
     * @return mixed
     */
    public function logout(Request $request)
    {
        Auth::logout();
        //Session::flush();
        $request->session()->flush();
        return redirect('/dashboard/');

    }

    protected function retriesLeft(Request $request)
    {
        return $this->maxLoginAttempts - app(RateLimiter::class)->attempts(app(Str::class)->lower($request->input('email')).'|'.$request->ip()) + 1;
    }

}