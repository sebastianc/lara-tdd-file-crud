<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(5),
        'body' => $faker->realText,
        'user_id' => $faker->numberBetween(1, 22),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
    ];
});
