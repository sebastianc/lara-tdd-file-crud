$( document ).ready(function() {
	"use strict";

    //Security Token
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
	
	// Detect OS
	if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('LINUX X86_64') >= 0 ) { 
		  $("body").addClass("mac");
		} else {
		  $("body").addClass("pc");
		}

	//Notification message hiding
	if($('.hide-after-delay')) {
		$('.hide-after-delay').fadeIn(450).delay(3200).fadeOut('slow');
	}

    //Notify the user on the status
    function createStatusMessage(message, success, id) {
        var className = success ? 'success_message' : 'error_message';
        var alert = $('<div></div>').text(message).addClass(className);
        $('#product').prepend(alert);
        $('#product').find('#'+id).remove();
        alert.fadeIn(450).delay(3200).fadeOut('slow');
    }
});