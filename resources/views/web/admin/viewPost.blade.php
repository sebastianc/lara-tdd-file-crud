@extends('web.layouts.plain')

@section('content')
    <div class="col" role="main">
        <div class="page-title bg-white col-xs-12">
            <div class="title_left">
                <h3 class="col-xs-12">{{date('F')}} {{date('Y')}}</h3>
            </div>
            <div class="title_right">
                <ul class="breadcrumb pull-right">
                    <li><a href="">View & Edit Post</a></li>
                    <li><a href="/dashboard">Dashboard</a></li>
                    <li><a href="/dashboard/logout">Log Out</a></li>
                </ul>
            </div>
        </div>
        <div class="x_panel">
            <div class="page-title page-title-inner">
                <div class="title_left">
                    <h3> {{$post->title}} </h3>
                </div>
                <div class="title_right"></div>
            </div>
            <div class="x_content">
                <form role="form" class="form-inline" action="{{ route('updatePost', $post->id) }}" method="post" enctype="multipart/form-data">
                    <button class="btn btn-purple-in pull-right margin-left submit-btn" type="submit">Save Changes</button>
                    <h4 class="grey-font col-sm-12">Delete</h4>
                    <div class="col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="delete">Delete post?</label>
                            <input type="checkbox" class="form-control switch" id="delete" name="delete">
                        </div>
                    </div>
                    <div class="col-sm-12 height1 border-bottom"></div>
                    <h4 class="grey-font col-sm-12">Details</h4>
                    <div class="col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" id="title" name="title" value="{{ $post->title }}" >
                        </div>
                        <div class="form-group">
                            <label for="body">Body</label>
                            <textarea class="form-control" id="body" name="body" value="{{ $post->body }}" >{{ $post->body }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="author">Author</label>
                            <span id="author" name="author">{{$post->user->name}}</span>
                        </div>
                    </div>
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                </form>
                <div class="col-sm-12 height1 border-bottom"></div>
            </div>
        </div>
    </div>
@endsection
