@extends('web.layouts.plain')

@section('content')
    <div class="container-fluid">
        <main role="main">
            <section class="login">
                <article class="col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-3 text-center">
                    <h4 class="purple ">Log in</h4>
                    <h3 class="text-capitalize text-center col-sm-12"></h3>
                    <form action="/dashboard/login" method="post">
                        <!-- In case of error please add class " has-error" in the following div -->
                        <div class="form-group">
                            <input type="email" name="email" class="form-control" placeholder="Email Address">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="Password">
                            {{--<a href="" class="col-sm-12">*Reset password</a> --}}
                        </div>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="submit" class="btn bt col-xs-12 white btn-purple-in" value="Log in"/>
                    </form>
                </article>

                <div class="col-xs-6 text-left">
                    <div class="previous">
                        <a href="/dashboard/register" class="btn bt col-xs-12 white btn-purple-in" >Register</a>
                    </div>
                </div>
                <div class="col-xs-6 text-right">
                    <div class="next">
                        <a href="/" class="btn bt col-xs-12 white btn-purple-in" >Back to Front Page</a>
                    </div>
                </div>
            </section>
        </main>
    </div>
@endsection
