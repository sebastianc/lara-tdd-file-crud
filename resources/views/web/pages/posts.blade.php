@extends('web.layouts.plainSky')

@section('content')
    <div class="col" role="main">
        <div class="page-title bg-white col-xs-12">
            <div class="title_left">
                <h3 class="col-xs-12">Where children put their dreams... {{date('F')}} {{date('Y')}}</h3>
            </div>
            <div class="title_right">
                <ul class="breadcrumb pull-right">
                    <li><a href="/">All Posts</a></li>
                    <li><a href="/dashboard">Dashboard</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="page-title page-title-inner">
                <div class="title_left">
                    <h3> Dream finder </h3>
                    <form class="col-md-7 col-sm-7 col-xs-12 form-group pull-left top_search"  action="{{ route('front') }}" method="get">
                        <div class="input-group">
                            <input type="text" name="s" class="form-control" placeholder="Search by name or text">
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">Go!</button>
                            </span>
                        </div>
                    </form>
                </div>
                <div class="title_right">
                </div>
            </div>
            <div class="x_content">
                @if(isset($noResults))
                    <section class="text-center">
                        <h3>No results found for "{{ $noResults }}".</h3>
                    </section>
                @else
                    @foreach($posts as $post)
                        <div class="col-lg-5 col-md-5 col-sm-12 cloud">
                            <h2>{{ $post->title }}</h2>
                            <div class="cloud_body">{{ $post->body }}</div>
                            {{-- For future author links $post->user->name --}}
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
        <!-- pagination -->
        <div class="col-sm-12">
            {{-- Future pagination $posts->links() --}}
        </div>
    </div>
@endsection
