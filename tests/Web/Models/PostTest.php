<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostTest extends TestCase
{
    public function testWhenUserCreatesANewPost()
    {
        $post = factory(\App\Post::class)->create();

        $this->assertTrue($post instanceof \App\Post);
        $this->assertDatabaseHas('posts', ['title' => $post->title, 'body' => $post->body, 'user_id' => $post->user_id]);
    }
}
