<?php

namespace Tests\Web\Models;

use Tests\TestCase;
use App\User;
use Illuminate\Support\Facades\Hash;
use Sofa\Eloquence\Eloquence;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{

    public function testPasswordIsHashed()
    {
        $user = new User();
        $user->password = 'test';
        $this->assertTrue(Hash::check('test', $user->password));
    }

    public function testEloquenceSmartSearchIsWorking()
    {
        $user = new User();
        $user->name = 'Charles Darwin';
        $user->username = 'Charles Darwin';
        $user->email = 'charles@darwin.com';
        $result = (array) User::search('Charles')->get();
        $prefix = chr(0).'*'.chr(0);
        $this->assertEquals(1,count($result[$prefix.'items']));
        $this->assertNotEmpty($result[$prefix.'items']);
    }

    public function testCreatePhoneVerificationCode()
    {
        $user = factory(\App\User::class)->create();

        $code = $user->createVerificationCode();
        $this->assertTrue($code instanceof \App\VerificationCode);
        $this->assertDatabaseHas('verification_codes', ['user_id' => $user->id, 'code' => $code->code]);

        //Make sure old codes are removed
        $code2 = $user->createVerificationCode();
        $this->assertDatabaseMissing('verification_codes', ['user_id' => $user->id, 'code' => $code->code]);
    }

    public function testWhenUserTriesToVerifyPhone()
    {
        $user = factory(\App\User::class)->create();
        $code = $user->createVerificationCode();
        $this->assertTrue($user->useVerificationCode($code->code));
        $this->assertTrue($user->phone_verified);
    }

    public function testWhenUserVerifiesPhoneWithAnInvalidCode()
    {
        $user = factory(\App\User::class)->create();
        $user->createVerificationCode();
        $this->assertFalse($user->useVerificationCode(211232));
        $this->assertNull($user->phone_verified);
    }

    public function testWhenAPasswordResetRequestIsCreated()
    {
        $user = factory(\App\User::class)->create();
        $reset = $user->createPasswordReset('127.0.0.1');
        $this->assertTrue($reset instanceof \App\PasswordReset);
        $this->assertDatabaseHas('password_resets', ['email' => $user->email]);
    }

    public function testWhenUserUtilizesThePasswordResetRequest()
    {
        $user = factory(\App\User::class)->create();
        $reset = $user->createPasswordReset('127.0.0.1');
        $this->assertDatabaseHas('password_resets', ['email' => $user->email]);
        $this->assertTrue($user->usePasswordReset($reset->token, 'newpassword'));

        // Trying to use the token again to see if the used flag is now true, should return false

        $this->assertFalse($user->usePasswordReset($reset->token, 'newpassword'));
    }

}
